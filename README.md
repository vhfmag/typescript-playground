# Typescript playground

This project is an attempt to provide a Typescript playground for different compiler options setups. Particularly, I want to use as a means to show how Typescript type syntax does not add functionality to the final transpiled code, being only a developer tool. As such, the default configuration does the bare minimum: remove Typescript syntax to provide the equivalent, untouched, Javascript code.

## To do

- [ ] Expose as GitLab pages
- [ ] Use React
- [ ] Allow user to change the configuration
- [ ] Persist configuration
- [ ] Show compiler errors
- [ ] Have a decent design