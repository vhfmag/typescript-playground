const gulp = require('gulp');
const path = require('path');
const livereload = require('gulp-livereload');
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const express = require('express');
const concat = require('gulp-concat');
const changed = require('gulp-changed');
const plumber = require('gulp-plumber');
const imagemin = require('gulp-image');
const browserify = require('gulp-bro');
const autoprefixer = require('gulp-autoprefixer');
const ts = require('gulp-typescript');
const sassImporter = require('sass-module-importer');

const tsProject = ts.createProject("tsconfig.json");
const serverPort = 5000;

const server = express();
server.use(express.static('./public'));

gulp.task('sass', function () {
    gulp.src('src/style/*.scss')
        .pipe(plumber())
        .pipe(changed('src/styles'))
        .pipe(sass({ includePaths: [path.resolve(__dirname, "node_modules")] }))
        .pipe(autoprefixer({
            browsers: ['> 1%'],
            cascade: false
        }))
        .pipe(gulp.dest('public/css'))
        .pipe(livereload());
});

gulp.task('ts', function () {
    gulp.src('src/scripts/*.ts')
        .pipe(plumber())
        .pipe(tsProject())
        .pipe(concat("bundle.js"))
        .pipe(gulp.dest('tmp'))
        .pipe(browserify())
        .pipe(gulp.dest('public/js'))
        .pipe(livereload());

});

gulp.task('html', function () {
    gulp.src('src/markup/**/[^_]*.pug')
        .pipe(plumber())
        .pipe(changed('public'))
        .pipe(pug())
        .pipe(gulp.dest('public'))
        .pipe(livereload());
});

gulp.task('img', function () {
    gulp.src('src/img/**/*.{png,jpeg,jpg,gif,svg}')
        .pipe(plumber())
        .pipe(changed('public/img'))
        .pipe(imagemin({
            pngquant: true,
            optipng: false,
            zopflipng: true,
            jpegRecompress: false,
            mozjpeg: true,
            guetzli: false,
            gifsicle: true,
            concurrent: 10
        }))
        .pipe(gulp.dest('public/img'))
        .pipe(livereload());
});

gulp.task('public', ['html', 'ts', 'sass', 'img']);

gulp.task('server', function () {
    server.listen(serverPort);
});

gulp.task('watch', function () {
    livereload.listen();
    gulp.watch('src/style/**/*.scss', ['sass']);
    gulp.watch('src/scripts/*.ts', ['ts']);
    gulp.watch('src/markup/**/*.pug', ['html']);
    gulp.watch('src/img/**/*.{png,jpeg,jpg,gif,svg}', ['img']);
});

gulp.task('default', ['public', 'server', 'watch']);