import * as ts from "typescript";
import { debounce } from "lodash";

const tsTextArea = document.querySelector("#tscode") as HTMLTextAreaElement | null;
const jsTextArea = document.querySelector("#jscode") as HTMLTextAreaElement | null;

const compilerOptions: ts.CompilerOptions = {
    module: ts.ModuleKind.ESNext,
    target: ts.ScriptTarget.ESNext,
    noEmitOnError: true,
};

function transpile(source: string) {
    return ts.transpile(source, compilerOptions);
}

const debouncedHandler = debounce(() => {
    console.log("debounced handler called");
    if (tsTextArea && jsTextArea) {
        jsTextArea.value = transpile(tsTextArea.value);
    }
}, 1000, { trailing: true });

console.log({ tsTextArea, jsTextArea });

if (tsTextArea && jsTextArea) {
    tsTextArea.addEventListener("input", () => {
        console.log("change triggered");
        debouncedHandler();
    });
}